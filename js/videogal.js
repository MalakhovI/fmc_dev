/**
 * Created by elenak on 05.12.16.
 */

var current_pict = '//img.youtube.com/vi/thZTZk09Cp4/default.jpg';
var current_text = $('.dp-video-main').siblings()[1].innerText;
var new_pict = '';
var new_text = '';

$('.qp-position-relative').on('click', function () {
    var pressed_img = this.children[0].attributes[0].value;
    var main_video = $('.dp-video-main');
    new_pict = pressed_img;
    new_text = $(this).parent().children()[2].innerText;
    this.children[0].attributes[0].value = current_pict;
    $(this).parent().children()[2].innerText = current_text;
    current_pict = new_pict;
    current_text = new_text;
    main_video.siblings()[1].innerText = new_text;
    switch (current_pict){
        case '//img.youtube.com/vi/thZTZk09Cp4/default.jpg':
            main_video.html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/thZTZk09Cp4?controls=0"></iframe>');
            break;
        case '//img.youtube.com/vi/e9GtPX6c_kg/default.jpg':
            main_video.html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/e9GtPX6c_kg?controls=0"></iframe>');
            break;
        case '//img.youtube.com/vi/NttGlw9ik6o/default.jpg':
            main_video.html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/NttGlw9ik6o?controls=0"></iframe>');
            break;
        case '//img.youtube.com/vi/IOi_JuSmGRM/default.jpg':
            main_video.html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/IOi_JuSmGRM?controls=0"></iframe>');
            break;
        case '//img.youtube.com/vi/awvqIi427_A/default.jpg':
            main_video.html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/awvqIi427_A?controls=0"></iframe>');
            break;
        default:
            main_video.html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/thZTZk09Cp4?controls=0"></iframe>');
            break;
    }
});